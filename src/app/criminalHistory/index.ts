import { Component, OnInit } from '@angular/core';
import { SantisukService } from '../service/santisuk.service';
import { ConfirmDialogService } from '../confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-index-criminal-history',
  templateUrl: './index.html',
  styleUrls: ['./css.css']
})
export class IndexCriminalHistoryComponent implements OnInit {
  filter:any = {fullName : ''}
  page:number = 1;
  constructor(
    private service : SantisukService,
    private confirm : ConfirmDialogService
  ) { }

  ngOnInit() {
    this.loadCriminals()
  }

  dataCriminals:any
  loadCriminals(){
    this.service.getCriminalsHistory().then(
      res => {
        //console.log(res);
        this.dataCriminals = res.data
      }
    )
  }

  photoShow:string = ''
  showImage(id){
    this.photoShow = id
  }

  deleteCriminal(id){
    this.confirm.confirm('DELETE', 'Do you want delete?')
    .then(
      (confirmed) => {
        if(confirmed){
          this.service.deleteCriminalHistory(id).then(
            res => {
              console.log("delete success")
              this.loadCriminals()
            }
          )
        }
      }
    ).catch(
      () => console.log('cancle')
    )
    
  }
}
