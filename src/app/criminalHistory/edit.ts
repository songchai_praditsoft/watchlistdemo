import { Component, OnInit } from '@angular/core';
import { NgModel, NgForm } from '@angular/forms';
import { SantisukService } from '../service/santisuk.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CountryService } from '../service/country.service';

@Component({
  selector: 'app-edit-criminal-history',
  templateUrl: './form.html',
  styleUrls: ['./css.css']
})
export class EditCriminalHistoryComponent implements OnInit {

  title:string = "แก้ไขข้อมูล"
  id:string
  dataCountry:any = this.country
  hide:boolean = false
  constructor(
    private service:SantisukService,
    private route:ActivatedRoute,
    private router:Router,
    private country:CountryService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.loadCriminal(this.id)
  }

  loadCriminal(id){
    this.service.getCriminalHistory(id).then(
      res => {
        this.criminalData.citizenId = res.data.citizenId
        this.criminalData.issuer = res.data.issuer
        this.criminalData.fullName = res.data.fullName
        this.criminalData.group = res.data.group
        this.criminalData.remarks = res.data.remarks
        console.log(res)
      }
    )
  }

  criminalData:any = {
    'citizenId' : '',
    'issuer' : 'Thailand',
    'fullName' : '',
    'group' : '',
    'remarks' : ''
  }

  FormCriminal(f:NgForm){
    this.criminalData.citizenId = f.value.citizenId
    this.criminalData.issuer = f.value.issuer
    this.criminalData.fullName = f.value.fullName
    this.criminalData.group = f.value.group
    this.criminalData.remarks = f.value.remarks

    console.log(this.criminalData)
    this.service.putCriminalHistory(this.criminalData,this.id).then(
      res => {
        console.log("edit succress")
        this.router.navigate(['/criminal'])
      }
    )
  }


}
