import { Component, OnInit } from '@angular/core';
import { SantisukService } from '../service/santisuk.service';

@Component({
  selector: 'app-show-criminal-history',
  templateUrl: './show.html',
  styleUrls: ['./css.css']
})
export class ShowCriminalHistoryComponent implements OnInit {

  constructor(
    private service : SantisukService
  ) { }

  ngOnInit() {
    this.loadCriminals()
  }

  dataCriminal:any
  loadCriminals(){
    this.service.getCriminalsHistory().then(
      res => {
        console.log(res);
        this.dataCriminal = res.data
      }
    )
  }
}
