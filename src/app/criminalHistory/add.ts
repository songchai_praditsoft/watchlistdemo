import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SantisukService } from '../service/santisuk.service';
import { Router } from '@angular/router';
import { CountryService } from '../service/country.service';

@Component({
  selector: 'app-add-criminal-history',
  templateUrl: './form.html',
  styleUrls: ['./css.css']
})
export class AddCriminalHistoryComponent implements OnInit {

  title:string = "เพิ่มข้อมูล"
  hide:boolean = true
  constructor(
    private service:SantisukService,
    private redirect:Router,
    private country:CountryService
  ) { }

  ngOnInit() {
    
  }

  criminalData:any = {
    'citizenId' : '',
    'issuer' : 'tha',
    'fullName' : '',
    'group' : 'group1',
    'remarks' : '',
    'faceImage' : ''
  }

  dataCountry:any = this.country

  FormCriminal(f:NgForm){
    this.criminalData.citizenId = f.value.citizenId
    this.criminalData.issuer = f.value.issuer
    this.criminalData.fullName = f.value.fullName
    this.criminalData.group = f.value.group
    this.criminalData.remarks = f.value.remarks

    const dataForm = new FormData();

    dataForm.append('citizenId',this.criminalData.citizenId)
    dataForm.append('issuer',this.criminalData.issuer)
    dataForm.append('fullName',this.criminalData.fullName)
    dataForm.append('group',this.criminalData.group)
    dataForm.append('remarks',this.criminalData.remarks)
    dataForm.append('file',this.filePicture,this.filePicture.name)

    console.log(this.criminalData)
    this.service.postCriminalHistory(dataForm).then(
      (res)=>{
        console.log("add success")
        this.redirect.navigate(['/criminal'])
      }
    )
  }

  filePicture:any
  pictureChange(picture){
    this.filePicture = picture.files[0]
  }

}
