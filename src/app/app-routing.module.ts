import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexCriminalHistoryComponent } from './criminalHistory';
import { ShowCriminalHistoryComponent } from './criminalHistory/show';
import { EditCriminalHistoryComponent } from './criminalHistory/edit';
import { AddCriminalHistoryComponent } from './criminalHistory/add';
import { LogsIndexComponent } from './logs/logs-index/logs-index.component';
import { LogsEditComponent } from './logs/logs-edit/logs-edit.component';
import { LogsAddComponent } from './logs/logs-add/logs-add.component';


const routes: Routes = [
  { path: '',   redirectTo: '/criminal', pathMatch: 'full' },
  { path: 'criminal' ,component:IndexCriminalHistoryComponent },
  { path: 'criminal/:id/show' ,component:ShowCriminalHistoryComponent },
  { path: 'criminal/:id/edit' ,component:EditCriminalHistoryComponent },
  { path: 'criminal/add' ,component:AddCriminalHistoryComponent },

  { path: 'logs' ,component:LogsIndexComponent },
  { path: 'logs/:id/edit' ,component:LogsEditComponent },
  { path: 'logs/add' ,component:LogsAddComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
