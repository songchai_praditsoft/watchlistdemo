import { Component, OnInit } from '@angular/core';
import { SantisukService } from 'src/app/service/santisuk.service';
import { ConfirmDialogService } from 'src/app/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-logs-index',
  templateUrl: './logs-index.component.html',
  styleUrls: ['./logs-index.component.css']
})
export class LogsIndexComponent implements OnInit {

  page:number = 1
  constructor(
    private service:SantisukService,
    private confirm:ConfirmDialogService
  ) { }

  ngOnInit() {
    this.loadLogs()
  }

  dataLogs:any
  loadLogs(){
    this.service.getLogs().then(
      res =>{
        this.dataLogs = res.data
        console.log(res.data)
      }
    )
  }

  imageShow:string = ''
  showImage(id){
    this.imageShow = id
  }

  deleteLog(id){
    this.confirm.confirm('DELETE', 'Do you want delete?')
    .then(
      (confirmed) => {
        if(confirmed){
          this.service.deleteLog(id).then(
            res => {
              console.log("delete success")
              this.loadLogs()
            }
          )
        }
      }
    ).catch(
      () => console.log('cancle')
    )
    
  }

}
