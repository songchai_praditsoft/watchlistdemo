import { Component, OnInit } from '@angular/core';
import { SantisukService } from 'src/app/service/santisuk.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CountryService } from 'src/app/service/country.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-logs-edit',
  templateUrl: '../logs-add/logs-add.component.html',
  styleUrls: ['../logs-add/logs-add.component.css']
})
export class LogsEditComponent implements OnInit {

  title:string = "แก้ไขข้อมูล"
  id:string
  dataCountry:any = this.country
  hide:boolean = false
  constructor(
    private service:SantisukService,
    private route:ActivatedRoute,
    private router:Router,
    private country:CountryService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.loadLog(this.id)
  }

  loadLog(id){
    this.service.getLog(id).then(
      res => {
        this.dataForm.inputDevice = res.data.inputDevice
        this.dataForm.docType = res.data.docType
        this.dataForm.issuer = res.data.issuer
        this.dataForm.addendum = res.data.addendum
        this.dataForm.location = res.data.location
        console.log(res)
      }
    )
  }

  dataForm:any = {
    'inputDevice' : 'keyboard',
    'docType' : 'card',
    'issuer' : 'tha',
    'addendum' : '',
    'location' : ''
  }
  FormLogs(f:NgForm){
    this.dataForm.inputDevice = f.value.inputDevice
    this.dataForm.docType = f.value.docType
    this.dataForm.issuer = f.value.issuer
    this.dataForm.addendum = f.value.addendum
    this.dataForm.location = f.value.location

    this.service.putLog(this.dataForm,this.id).then(
      res => {
        console.log("edit succress")
        this.router.navigate(['/logs'])
      }
    )
  }
}
