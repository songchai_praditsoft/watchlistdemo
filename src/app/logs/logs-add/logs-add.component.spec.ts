import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsAddComponent } from './logs-add.component';

describe('LogsAddComponent', () => {
  let component: LogsAddComponent;
  let fixture: ComponentFixture<LogsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
