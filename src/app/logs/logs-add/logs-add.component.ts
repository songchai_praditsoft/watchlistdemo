import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CountryService } from 'src/app/service/country.service';
import { SantisukService } from 'src/app/service/santisuk.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logs-add',
  templateUrl: './logs-add.component.html',
  styleUrls: ['./logs-add.component.css']
})
export class LogsAddComponent implements OnInit {
  title:string = "Add Logs"
  dataCountry:any = this.country
  hide:boolean = true

  constructor(
    private country:CountryService,
    private service:SantisukService,
    private router:Router
  ) { }

  ngOnInit() {
    
  }

  dataForm:any = {
    'inputDevice' : 'keyboard',
    'docType' : 'card',
    'issuer' : 'tha',
    'addendum' : '',
    'location' : ''
  }
  FormLogs(f:NgForm){
    this.dataForm.inputDevice = f.value.inputDevice
    this.dataForm.docType = f.value.docType
    this.dataForm.issuer = f.value.issuer
    this.dataForm.addendum = f.value.addendum
    this.dataForm.location = f.value.location

    console.log(this.dataForm)
    const formData = new FormData();

    formData.append('inputDevice',this.dataForm.inputDevice)
    formData.append('docType',this.dataForm.docType)
    formData.append('issuer',this.dataForm.issuer)
    formData.append('addendum',this.dataForm.addendum)
    formData.append('location',this.dataForm.location)
    formData.append('file',this.filePicture,this.filePicture.name)

    this.service.postLog(formData).then(
      res => {
        this.router.navigate(['/logs'])
      }
    )

  }

  filePicture:any
  pictureChange(picture){
    this.filePicture = picture.files[0]
  }
}
