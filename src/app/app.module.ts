import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddCriminalHistoryComponent } from './criminalHistory/add';
import { EditCriminalHistoryComponent } from './criminalHistory/edit';
import { ShowCriminalHistoryComponent } from './criminalHistory/show';
import { IndexCriminalHistoryComponent } from './criminalHistory';
import { FormsModule } from '@angular/forms';
import { SantisukService } from './service/santisuk.service';
import { HttpClientModule } from '@angular/common/http'; 
import { CountryService } from './service/country.service';
import { FilterPipeModule } from 'ngx-filter-pipe'
import { NgxPaginationModule } from 'ngx-pagination'
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { ConfirmDialogService } from './confirm-dialog/confirm-dialog.service';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { LogsIndexComponent } from './logs/logs-index/logs-index.component';
import { LogsEditComponent } from './logs/logs-edit/logs-edit.component';
import { LogsAddComponent } from './logs/logs-add/logs-add.component';


@NgModule({
  declarations: [
    AppComponent,
    AddCriminalHistoryComponent,
    EditCriminalHistoryComponent,
    ShowCriminalHistoryComponent,
    IndexCriminalHistoryComponent,
    ConfirmDialogComponent,
    LogsIndexComponent,
    LogsEditComponent,
    LogsAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    FilterPipeModule,
    NgxPaginationModule,
    NgbModalModule
  ],
  providers: [
    SantisukService,
    CountryService,
    ConfirmDialogService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ ConfirmDialogComponent ],
})
export class AppModule { }
