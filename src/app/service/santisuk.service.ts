import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SantisukService {

  base_url:string = "http://192.168.1.100/api/santisuk/"
  constructor(
    private http:HttpClient
  ) { }

  getCriminalsHistory():Promise<any>{
    return this.http.get(this.base_url+'watchlists/').toPromise()
  }

  getCriminalHistory(id):Promise<any>{
    return this.http.get(this.base_url+'watchlists/'+id).toPromise()
  }

  postCriminalHistory(data):Promise<any>{
    let header = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };
    return this.http.post(this.base_url+'watchlists/',data).toPromise()
  }

  putCriminalHistory(data,id):Promise<any>{
    let header = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };
    return this.http.put(this.base_url+'watchlists/'+id,data,header).toPromise()
  }

  deleteCriminalHistory(id):Promise<any>{
    return this.http.delete(this.base_url+'watchlists/'+id).toPromise()
  }

  //logs
  getLogs():Promise<any>{
    return this.http.get(this.base_url+'logs').toPromise()
  }

  getLog(id):Promise<any>{
    return this.http.get(this.base_url+'logs/'+id).toPromise()
  }

  postLog(data):Promise<any>{
    let header = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };
    return this.http.post(this.base_url+'logs',data).toPromise()
  }

  putLog(data,id):Promise<any>{
    let header = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };
    return this.http.put(this.base_url+'logs/'+id,data,header).toPromise()
  }

  deleteLog(id):Promise<any>{
    return this.http.delete(this.base_url+'logs/'+id).toPromise()
  }
}
